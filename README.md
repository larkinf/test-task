# Тестовое задание

v0.2: 
- Логи отправил в stdout (если нужны логи в фс, раскомменчиваем нужные строки в docker-compose и комментим те, что с stdout);

- worker_processes (nginx) выставил в auto



Поднимаем 3 контейнера, 1 с балансировщиком (NGINX) и 2 с вебом (helloworld.py)

>cd ~/ && git clone https://gitlab.com/larkinf/test-task.git && cd ./test-task && docker-compose build && docker-compose up -d

Тестировалось на:

*CentOS 7.4.1708*

*Docker version 18.06.1-ce-rc1, build 0928140*

*docker-compose version 1.22.0, build f46880fe*
